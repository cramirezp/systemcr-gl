from tkinter import *

#import tkMessageBox
import matplotlib.pyplot as plt
import obspy
from obspy.core.util import NamedTemporaryFile
from obspy.clients.fdsn import Client as FDSN_Client
from obspy.clients.iris import Client as OldIris_Client
#from ScrolledText import *
from peticion_web import *
from OpenFile_multiple import *
import decimal
import random
import math
from Guardar_datos import *
#from tkFileDialog import askopenfilename
from Lectura_file import *
from Rutas import *
from Pos_Analisis_Resultados import *
from ReName import *
import os
from shutil import rmtree
from scross import *
from Dia_juliano import *

class Solicitud_web:

	def trabajar(self):
		self.year_an = (self.entrada_texto.get())


		try:
			self.month = int(self.month.get())
			valido = True
			#VALIDAR FACTOR DE CORRECION TENGA VALOR DENTRO DEL RANGO
			if self.month>0 and self.month<=12:
				valido = True
			else:
				valido = False
				tkMessageBox.showinfo("Error", "Error", "Debe introducir un valor entre 1 and 12")

		except (Exception, e):
			valido = False
			tkMessageBox.showinfo("Error", "Debe introducir un valor entre 1 and 12")		
		


		self.day = (self.day.get())
		#self.day_end= (self.day_end.get())
		#self.month = (self.month.get())
		#self.month_end = (self.month_end.get())
		#self.Channel = (self.Channel.get())
		#self.Channel=self.Channel.upper()
		self.Channel='HHZ'
		#self.station = (self.station.get())
		#self.station=self.station.upper()
		self.code = (self.code.get())
		self.code = self.code.upper()

		self.day=str(self.day)
		self.month=str(self.month)
		
		if len(self.day)<2:
			self.day='0'+str(self.day)
		else:
			self.day=str(self.day)

		if len(self.month)<2:
			self.month='0'+str(self.month)
		else:
			self.month=str(self.month)


		
		inicia_peticiones=peticion_web()
		inicio_dia_j=Dia_juliano()
		inicia_xcross=scross()
		day_j=inicio_dia_j.main(self.year_an, self.month,self.day)
		print ("dia julino", day_j)
		day_j=str(day_j)
		if len(day_j)<3:
			day_j='0'+day_j
		print ('==============================================')
		dias_analisis=[]

		#============================ Se indica path para guardar resultados ===============================
		inicia=Guardar_datos()
		Tipo="Select file for salve results"
		#ruta_guardar_datos_end=inicia.guardar_directorio(Tipo)
		ruta_guardar_datos_end="/home/carlos/Documentos"
		ruta_guardar_datos=ruta_guardar_datos_end+"/results_pre"
		print ('==============================================')
		print ( " path para guardar datos", ruta_guardar_datos)
		#datos_rep[4]=ruta_guardar_datos
		estaciones=self.estaciones
		self.ruta_guardar_rep_datos=ruta_guardar_datos+'/tmp'
		if not os.path.exists(self.ruta_guardar_rep_datos):
			os.makedirs(self.ruta_guardar_rep_datos)
		



		# for sta in range(len(self.estaciones)):
		# 	#sta=3
		# 	#self.pathArchivoDE=[]
		# 	aux_rep=[]
		# 	print (self.year_an, self.day, self.Channel, self.estaciones[sta], self.month, self.code)
		# 	datos_rep=[self.month, self.day,self.code, self.estaciones[sta],""]
		# 	st_z=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], self.Channel)
		# 	st_e=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], 'HHE')
		# 	st_n=inicia_peticiones.main( self.year_an, self.month, self.day, self.code, self.estaciones[sta], 'HHN')
		# 	#aux_rep.append(st_z)
		# 	name_rep_z=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHZ.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
		# 	name_rep_e=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHE.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
		# 	name_rep_n=self.ruta_guardar_rep_datos+'/'+self.code+'.'+self.estaciones[sta]+'..HHN.M.'+str(self.year_an)+'.'+str(day_j)+'.000000.SAC'
		# 	#print ("name_rep", name_rep)
		# 	#aux_rep.append(name_rep)
		# 	if st_z!= None and st_e != None and st_n != None:
		# 		st_z.write(name_rep_z, format='SAC')
		# 		st_e.write(name_rep_e, format='SAC') 
		# 		st_n.write(name_rep_n, format='SAC')

		
		if not os.path.exists(ruta_guardar_datos):
			os.makedirs(ruta_guardar_datos)
		
		#self.pathArchivoDE.append(st)
		
		#***********************
		self.abrirArchivo()		
		respuesta=self.validar()		
		if respuesta == True:
			for i in range(len(self.pathArchivoDE)):

				for ii in estaciones:
					if self.pathArchivoDE[i].find(ii) !=-1:
						self.estacion_1=ii

				#=========================================
				for j in range(len(self.pathArchivoDF)):

					for jj in estaciones:
						if self.pathArchivoDF[j].find(jj) != -1:
							self.estacion_2=jj
					if self.estacion_1==self.estacion_2:
						dia_aux=inicia_xcross.main( self.pathArchivoDE[i], self.pathArchivoDF[j], ruta_guardar_datos, estaciones, self.year_an)
						#verifica los dias analizados por el sistema y lo guarda en el vector dias analisis
						if dia_aux not in dias_analisis:
							if dia_aux!= None:
								dias_analisis.append(dia_aux)

			self.app.destroy()

		print ("--- ", dias_analisis, "----")
		inicia_pos_analisis=Pos_Analisis_Resultados()
		#print "++", ruta_guardar_datos_end, "++", d
		for d in dias_analisis:
			print ("++", ruta_guardar_datos_end, "++", d)
			inicia_pos_analisis.main(ruta_guardar_datos_end,ruta_guardar_datos, d, estaciones, self.ruta_repository, self.year_an)
		#inicia=ClasificaPrueba()
		#inicia.main(self.pathArchivoDE, self.pathArchivoDF)
		inicia_rename=ReName()
		inicia_rename.main(self.year_an, estaciones, ruta_guardar_datos, ruta_guardar_datos_end, dias_analisis)
		#rmtree(ruta_guardar_datos_end)



			
			



	def validar(self):
		valido = False
		##print  'entra'

		#VALIDAR QUE EXITAN PATH SI NO SE SELECCIONO MANUAL
		#if self.pathArchivoDE == None and manualDE == False:
		if self.pathArchivoDE == None:
			valido = False
			tkMessageBox.showinfo("Error", "Debe seleccionar el primer conjunto  de datos de entrada")
		elif self.pathArchivoDF == None and manualDES == False:
			valido = False
			tkMessageBox.showinfo("Error", "Debe seleccionar el segundo conjunto de datos de entrada")
		else:
			valido = True

		return valido
	def abrirArchivo(self):
		#print ':)****'
		file=self.ruta_guardar_rep_datos
		print (file)
		archivos=os.listdir(file)
		archivos=sorted(archivos)
		print (archivos)
		directorios=[]
		path=[]
		aux_return=[]
		for i in range(len(archivos)):
			archivos[i]= file+'/'+archivos[i]

			print(i,':  ', archivos[i])
			#if os.path.isdir(archivos[i])==True:
				#temp=os.listdir(archivos[i])
				#temp=sorted(temp)
				#print ("temp", temp)
				#for j in range(len(temp)):
			if archivos[i].find('HHZ')!=-1:
				path.append(archivos[i])
		aux_return=[path, file]
		self.ruta_repository=aux_return[1]
		print (path, "++++")
		archivo =aux_return[0]
		
		self.pathArchivoDE=archivo
		print  (self.pathArchivoDE)

	def pathfileDF(self):
		Tipo=" Select files in format SAC calog"
		inicia=OpenFile_multiple()

		self.filename = inicia.main(Tipo)
		return(self.filename[0])
	def abrirArchivoFile(self):

		archivo =self.pathfileDF()
		self.etiquetaPath1.config(text="Select files:"+str(len(archivo)))
		self.pathArchivoDF=archivo
		#print  self.pathArchivoDF

	def abrirArchivoStation(self):

		inicio_path_sta=Rutas()
		Tipo=" Select files with stations"
		path_sta=inicio_path_sta.main(Tipo)
		inicio=Lectura_file()
		#path="/home/carlos/Dropbox/Implementacion_DOC/examples/stations.txt"
		a=inicio.main(path_sta)
		aux=[]
		for i in range(len(a)):
			if i!=0:
				aux.append(a[i][0])
		self.estaciones=aux
		print (self.estaciones)

		#archivo =self.pathfileDF()
		self.etiquetaPath2.config(text="Select files:"+str(len(self.estaciones)))
		#self.pathArchivoSta=archivo
		#print  self.pathArchivoDF
	def pathfileDF(self):
		Tipo=" Select files in format SAC calog"
		inicia=OpenFile_multiple()

		self.filename = inicia.main(Tipo)
		return(self.filename[0])		
	def main(self):
		valor = "" #para el inicio los entry tenga b

		self.app = Tk()
		self.app.title('Input of data for correlation')
		self.app.geometry("1000x300")
		self.app.maxsize(800, 300)

		#VP -> VENTANA PRINCIPAL
		vp = Frame(self.app)
		
		vp.grid(column=0, row=0, padx=(70,70), pady=(20,20)) #posicionar los elementos en tipo matriz, padx es para margen
		vp.columnconfigure(0,weight=1)  #tamanio relativo a las columnas
		vp.rowconfigure(0,weight=1)


		#DATOS ENTRADA POR PATH
		
		etiqueta = Label(vp,text="year: ")
		etiqueta.grid(column=0, row=4) 


		self.entrada_texto = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.entrada_texto.grid(column=1,row=4)

		#**********************************************************************************************
		#FDSN code
		etiqueta = Label(vp,text="FDSN code")
		etiqueta.grid(column=0, row=5) 
		self.code= Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.code.grid(column=1,row=5)

		#***********************************************************************************************
		#NUMERO DE NEURONAS
		#etiqueta = Label(vp,text="Numero: ")
		#etiqueta.grid(column=0, row=6) 
		
		#self.NoNeurona = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		#self.NoNeurona.grid(column=1,row=6)



		#NUMERO ITERACIONES
		etiqueta = Label(vp,text="Initial month: ")
		etiqueta.grid(column=0, row=7) 
		
		self.month = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.month.grid(column=1,row=7)		#self.entradaIter.config(state=DISABLED)
		#ERROR CUADRATICO MEDIO
		etiqueta = Label(vp,text="Initial day: ")
		etiqueta.grid(column=0, row=8) 
		#
		self.day = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.day.grid(column=1,row=8)


		#self.entradaMSE.config(state=DISABLED)

		etiquetaDF = Label(vp,text="Select set data ")
		etiquetaDF.grid(column=0, row=1) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select catalog (masters)", command=self.abrirArchivoFile)
		boton.grid(column=2,row=1)
 	

		etiquetaDF = Label(vp,text="Select set stations ")
		etiquetaDF.grid(column=0, row=0) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select file stations", command=self.abrirArchivoStation)
		boton.grid(column=2,row=0)

		

		

		self.etiquetaPath1 = Label(vp,text=" ")
		self.etiquetaPath1.grid(column=1, row=1) 
		
		self.etiquetaPath2 = Label(vp,text=" ")
		self.etiquetaPath2.grid(column=1, row=0) 


		
		# #EJECUCION
		boton = Button(vp, width=20, text="Run", command=self.trabajar)
		boton.grid(column=1,row=12)


	


		botonSalir = Button(vp, width=20, text="Quit", command=self.app.destroy)
		botonSalir.grid(column=1,row=13)
		


		#CREDITOS
		etiqueta = Label(vp,text="Elaborado por: CARLOS RAMIREZ PINA")
		etiqueta.grid(column=1, row=40) 
		self.app.mainloop()






