
#====================Librerias===================================================
import tkinter.filedialog
from tkinter import *
from tkinter import Tk
import os
import os.path

#====================Librerias===================================================



class OpenFile_multiple():
	def main(self, tipo):
		
		file=tkinter.filedialog.askdirectory(initialdir = "/home",title = tipo)
		archivos=os.listdir(file)
		archivos=sorted(archivos)
		directorios=[]
		path=[]
		aux_return=[]
		for i in range(len(archivos)):
			archivos[i]= file+'/'+archivos[i]
			if os.path.isdir(archivos[i])==True:
				temp=os.listdir(archivos[i])
				temp=sorted(temp)
				for j in range(len(temp)):
					if temp[j].find('HHZ')!=-1:
						path.append(archivos[i]+'/'+temp[j])
		aux_return=[path, file]
		return aux_return


#inicia=OpenFile_multiple()
#inicia.main()