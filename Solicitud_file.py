import tkinter as tk
from tkinter import ttk
from Solicitud_web import *
from Datos_correlacion import *

class Solicitud_file():

	def web(self):
		print("funcion web")
		self.root.destroy()		
		incio_web=Solicitud_web()
		incio_web.main()
	def local(self):
		print("funcion local")
		self.root.destroy()
		Inicia_D_C=Datos_correlacion()
		Inicia_D_C.main()

	def cancelar(self):
	    print("cancelar")
	    self.root.destroy()

	def main(self):

	
		self.root = tk.Tk()
		self.root.config(width=260, height=130)
		self.root.title("Botón en Tk")
		boton = ttk.Button(text="Download files from the web", command=self.web)
		boton.place(x=10, y=10)
		boton1 = ttk.Button(text="Files locally", command=self.local)
		boton1.place(x=10, y=50)
		boton2 = ttk.Button(text="Cancel", command=self.cancelar)
		boton2.place(x=10, y=90)
		self.root.mainloop()

