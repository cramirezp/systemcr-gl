#proyecto de tesis de Phd
#Desarrollado por Carlos Ramirez Pina
#====================Librerias===================================================
import sys
from tkinter import *
import tkinter.filedialog
from tkinter import messagebox
#from ScrolledText import *
#import ttk
from OpenFile_multiple import *
from scross import *
import decimal
import random
import math
from Guardar_datos import *
from tkinter.filedialog import askopenfilename
from Lectura_file import *
from Rutas import *
from Pos_Analisis_Resultados import *
from ReName import *
import os
from shutil import rmtree
#====================Librerias===================================================

class Datos_correlacion():
	print ("welcome to system for egenerate calog of the events seismic")
	filename = ""
	pathArchivoDE=None
	pathArchivoDF=None
	def pathfileDE(self):
		Tipo=" Select files in format SAC repository"
		inicia_guardando=Guardar_datos()


		inicia=OpenFile_multiple()

		self.filename = inicia.main(Tipo)
		#print "self.filename----", self.filename[1]
		self.ruta_repository=self.filename[1]
		return(self.filename[0])
	def pathfileDF(self):
		Tipo=" Select files in format SAC calog"
		inicia=OpenFile_multiple()

		self.filename = inicia.main(Tipo)
		return(self.filename[0])
	def abrirArchivo(self):
		#print ':)****'
		
		archivo =self.pathfileDE()
		self.etiquetaPath.config(text="Select files:"+str(len(archivo)))
		self.pathArchivoDE=archivo
		#print  self.pathArchivoDE
		
	def abrirArchivoFile(self):

		archivo =self.pathfileDF()
		self.etiquetaPath1.config(text="Select files:"+str(len(archivo)))
		self.pathArchivoDF=archivo
		#print  self.pathArchivoDF

	def abrirArchivoStation(self):

		inicio_path_sta=Rutas()
		Tipo=" Select files with stations"
		path_sta=inicio_path_sta.main(Tipo)
		inicio=Lectura_file()
		#path="/home/carlos/Dropbox/Implementacion_DOC/examples/stations.txt"
		a=inicio.main(path_sta)
		aux=[]
		for i in range(len(a)):
			if i!=0:
				aux.append(a[i][0])
		self.estaciones=aux
		#print self.estaciones

		#archivo =self.pathfileDF()
		self.etiquetaPath2.config(text="Select files:"+str(len(self.estaciones)))
		#self.pathArchivoSta=archivo
		#print  self.pathArchivoDF



	def trabajar(self):
		#print  ":)"
		dias_analisis=[]
		self.year_an = float(self.entrada_texto.get())
		self.year_an=int(self.year_an)
		respuesta=self.validar()
		if respuesta == True:
			#============================ Se indica path para guardar resultados ===============================
			inicia=Guardar_datos()
			Tipo="Select file for salve results"
			ruta_guardar_datos_end=inicia.guardar_directorio(Tipo)
			ruta_guardar_datos=ruta_guardar_datos_end+"/results_pre"
			print ('==============================================')
			print (" path para guardar datos", ruta_guardar_datos)

			if not os.path.exists(ruta_guardar_datos):
				os.makedirs(ruta_guardar_datos)

			print ('==============================================')
			inicia_xcross=scross()
			estaciones=self.estaciones
			#estaciones=['ALPI', 'BAVA', 'CANO', 'CDGZ', 'COLM', 'COMA', 'CUAT','EBMG', 'ESPN', 'GARC','HIGA', 'JANU', 'MAZE', 'MORA', 'OLOT', 'PAVE', 'PERC', 'SANM', 'SCRI', 'SINN', 'SNID', 'ZAPO']
			
			for i in range(len(self.pathArchivoDE)):

				for ii in estaciones:
					if self.pathArchivoDE[i].find(ii) !=-1:
						self.estacion_1=ii

				#=========================================
				for j in range(len(self.pathArchivoDF)):

					for jj in estaciones:
						if self.pathArchivoDF[j].find(jj) != -1:
							self.estacion_2=jj
					if self.estacion_1==self.estacion_2:
						dia_aux=inicia_xcross.main( self.pathArchivoDE[i], self.pathArchivoDF[j], ruta_guardar_datos, estaciones, self.year_an)
						#verifica los dias analizados por el sistema y lo guarda en el vector dias analisis
						if dia_aux not in dias_analisis:
							if dia_aux!= None:
								dias_analisis.append(dia_aux)

			self.app.destroy()

		print ("--- ", dias_analisis, "----")
		inicia_pos_analisis=Pos_Analisis_Resultados()
		#print "++", ruta_guardar_datos_end, "++", d
		for d in dias_analisis:
			print ("++", ruta_guardar_datos_end, "++", d)
			inicia_pos_analisis.main(ruta_guardar_datos_end,ruta_guardar_datos, d, estaciones, self.ruta_repository, self.year_an)
		#inicia=ClasificaPrueba()
		#inicia.main(self.pathArchivoDE, self.pathArchivoDF)
		inicia_rename=ReName()
		inicia_rename.main(self.year_an, estaciones, ruta_guardar_datos, ruta_guardar_datos_end, dias_analisis)
		#rmtree(ruta_guardar_datos_end)



	

	def validar(self):
		valido = False
		##print  'entra'

		#VALIDAR QUE EXITAN PATH SI NO SE SELECCIONO MANUAL
		#if self.pathArchivoDE == None and manualDE == False:
		if self.pathArchivoDE == None:
			valido = False
			tkMessageBox.showinfo("Error", "Debe seleccionar el primer conjunto  de datos de entrada")
		elif self.pathArchivoDF == None and manualDES == False:
			valido = False
			tkMessageBox.showinfo("Error", "Debe seleccionar el segundo conjunto de datos de entrada")
		else:
			valido = True

		return valido


	def habilitarManual(self):
		if len(self.b)!=0:
			self.botonGuardar.config(state=NORMAL)



	def habilitarDataSet(self, long):
		if long !=0:
			self.entrada_texto.config(state=NORMAL)
			self.entradaIter.config(state=NORMAL)
			self.entradaMSE.config(state=NORMAL)
			self.botonEntrenar.config(state=NORMAL)


		# cadena = "Factor de correccion: " + str(alfa) + "\n" + "Error minimo esperado: " + str(errorminimo) + "\n" + "Error Promedio: " + str(errorpromedio) + "\n"+ "Iteracion: " + str(iteracion) + "\n" + "Error minimo: " + str(minimo) + "\n" + "r1: " + str(ww11) + "\n" + "r2: " + str(uu) + "\n" + "bias: " + str(bb1) + "\n"	+ "bias2:"  + str(bb3) + "\n"
		# result.config(state=NORMAL)
		# result.insert(INSERT, cadena)
		# result.config(state=DISABLED)
	def cambiar_stringvar(nuevotexto,stringvar):
		stringvar.set(nuevotexto)



	def main(self):
		valor = "" #para el inicio los entry tenga b

		self.app = Tk()
		self.app.title('Input of data for correlation')
		self.app.geometry("736x200")
		self.app.maxsize(736, 200)

		#VP -> VENTANA PRINCIPAL
		vp = Frame(self.app)
		
		vp.grid(column=0, row=0, padx=(50,50), pady=(10,10)) #posicionar los elementos en tipo matriz, padx es para margen
		vp.columnconfigure(0,weight=1)  #tamanio relativo a las columnas
		vp.rowconfigure(0,weight=1)


		#DATOS ENTRADA POR PATH
		etiquetaDE = Label(vp,text="Set data: ")
		etiquetaDE.grid(column=0, row=0) #especificar en la final y columna 
		
		boton = Button(vp, width=21, text="Select for day (repository)", command=self.abrirArchivo)
		boton.grid(column=2,row=0)

		etiquetaDF = Label(vp,text="Select set data ")
		etiquetaDF.grid(column=0, row=2) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select catalog (masters)", command=self.abrirArchivoFile)
		boton.grid(column=2,row=2)
 	

		etiquetaDF = Label(vp,text="Select set stations ")
		etiquetaDF.grid(column=0, row=3) #especificar en la final y columna 
	
		boton = Button(vp, width=21, text="Select file stations", command=self.abrirArchivoStation)
		boton.grid(column=2,row=3)

		etiqueta = Label(vp,text="year of analysis: ")
		etiqueta.grid(column=0, row=4) 
		self.entrada_texto = Entry(vp,width=20,textvariable=valor) #unidaes relativas
		self.entrada_texto.grid(column=1,row=4)

		self.etiquetaPath = Label(vp,text=" ")
		self.etiquetaPath.grid(column=1, row=0) 

		self.etiquetaPath1 = Label(vp,text=" ")
		self.etiquetaPath1.grid(column=1, row=2) 
		
		self.etiquetaPath2 = Label(vp,text=" ")
		self.etiquetaPath2.grid(column=1, row=3) 
		
		# #EJECUCION
		boton = Button(vp, width=20, text="Run", command=self.trabajar)
		boton.grid(column=1,row=8)


	


		botonSalir = Button(vp, width=20, text="Quit", command=self.app.destroy)
		botonSalir.grid(column=2,row=22)
		


		#CREDITOS
		etiqueta = Label(vp,text="Elaborado por: CARLOS RAMIREZ PINA")
		etiqueta.grid(column=1, row=40) 
		self.app.mainloop()


#Inicia_D_C=Datos_correlacion()
#Inicia_D_C.main()

