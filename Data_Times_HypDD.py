import os.path
import os.path
import subprocess
import glob
from datetime import datetime
from Lectura_file import *
from Correlacion import *
from Rutas import *
from Rutas_Dir import *
from tkinter import *
#import tkFileDialog
import tkinter as tk
from file_hypodd import *
from times_seg import *
class Data_Times_HypDD():


	def main_data_times_hyp(self, mode, opc):
		#model='codex_16'
		self.opc=opc
		self.model=mode
		print ("Bienvenidos al Software file input and out file HypoDD con python")

		print ("**********************")
		print ("Sistema actualizado")
		print ("**********************")

		print ("Ingresa el nombre del modelo a ser analizado:")
		#model=""
		#model=raw_input()
		#print type(self.model)
		self.archivos_origen=self.origen(self.model)
		#print "****orig"
		self.archivos_origen.pop(0)
		#for i in self.archivos_origen:
			#print i
		pregunta_b=0
		self.main_pregunta(pregunta_b)

	def datos_proceso(self, archivos_pick):
		#archivos_pick=self.Lec_files()

		#archivos_origen=self.origen(model)
		#print "****origen"
		#archivos_pick=self.Read_files()

		#print "****read"
		
		
		#print len(archivos_pick)
		#print len(archivos_cross)
		print ("Nombre del modelo:  ", self.model)
		inicio_rutas_dir=Rutas_Dir()
		Tipo="Input path to salve files times"
		path_salve=inicio_rutas_dir.pathfile(Tipo)
		path_salve=path_salve+'/datos_time/'+self.model+'/'
		for i in range(len(self.archivos_origen)):
			idece_analizar=int(self.archivos_origen[i][0])
			self.Proceso(archivos_pick[idece_analizar], self.archivos_origen[i], self.model, path_salve )

		print ("************************")
		print ("Fin de proceso uno")
		print ("************************")
		messagebox.showinfo(message="process completed successfully", title="Message")
		#inicio_hypDD=file_hypodd()
		#inicio_hypDD.data_times(opc=2)
	
						
		#resp=raw_input("Deseas realizar otra proceso (Yes), ingresa cualquier valor y enter para terminar:  ")

	def origen(self, model):	
		#model='codex_13'
		inicio_rutas=Rutas()
		Tipo="Input path "+model+" localization"
		ruta_loca=inicio_rutas.main(Tipo)

		#ruta_loca='/home/carlos/Dropbox/Catalogo_2006_2007/'+model+'.txt'
		f = open(ruta_loca,"r")
		elementos= []
		while True:
			linea = f.readline()
			if linea:
				elementos.append(linea)
			else:
						break

		elementos1 = []
		for i in elementos:
			lista=i.split("\t")
			lista.remove("\n")
			elementos1.append([(e) for e in lista])
		
		archivos=[]


		#for i in elementos1:
			#aux=[]
			#print i[0], i[2], i[3]
		
		return elementos1	
	def main_pregunta(self, b):
		if b==0:
			self.a=[]
			self.pregunta()
		elif b==1:
			self.pregunta()
		else:
			print (self.a)
			self.Lec_files(self.a)

	def Pregunta_yes(self):
	    inicio_rutas_dir=Rutas_Dir()
	    self.root3.destroy()
	    Tipo="Path Directory whit files phases"
	    #Tk().withdraw()
	    file=inicio_rutas_dir.pathfile(Tipo)
	    #borrar()
	    self.a.append(file+'/')
	    b=1
	    self.main_pregunta(b)


	def Pregunta_not(self):
	    self.root3.destroy()
	    b=2
	    self.main_pregunta(b)


	def pregunta(self):

		a=[]
		self.root3 = Tk()
		#self.root3.config(bd=25)
		self.root3.title(" Input name to identifier")
		T = Text(self.root3,  height = 1, width = 30)

		Fact = """Select directory with files"""

		T.pack()
		Button(self.root3, text="yes", command=self.Pregunta_yes).pack(side="left")
		#Button(root, text="Resta", command=resta).pack(side="left")
		Button(self.root3, text="Not", command=self.Pregunta_not).pack(side="right")

		T.insert(tk.END, Fact)
		self.root3.mainloop()

	def Lec_files(self,a):
		

		files_all=a
		#print "recibe los valores: ", files_all

		#files_all=['/home/carlos/Dropbox/Catalogo_2006_2007/Phases_2006/','/home/carlos/Dropbox/Catalogo_2006_2007/Phases_2007/']
		seq=0		
		path_archivos=[]
		for fi in files_all:
			file=fi
			archivos=os.listdir(file)

			archivos=sorted(archivos)
			#path_archivos+=archivos
			#print 'Archivos: \n', archivos
			aux=[]
			for arh in range(len(archivos)):
				i=archivos[arh]
				aux.append(file+i)
			path_archivos+=aux	

		#print path_archivos
		#print "proceso datos proceso"
		self.datos_proceso(path_archivos)	
			
		#return path_archivos

	def Proceso(self, i, origen, model, path_salve):
		#funcion para las nuevas modificaciones
		

		inicio_read_file=Lectura_file()
		arreglo=[]
		arreglo=inicio_read_file.main(i)
		#print origen
		time_origen=origen[3]
		if time_origen[5]==' ':
			aux_o1=time_origen[0:4]
			aux_o2=time_origen[6:len(time_origen)]
			time_origen=aux_o1+'0'+aux_o2
			#print 'aqui', time_origen
		time_origen=time_origen.replace(' ', '')
		#print 'cambiado', time_origen
		origen_hora=int(time_origen[0:2])
		origen_min=int(time_origen[2:4])
		origen_seg=int(time_origen[len(time_origen)-5:len(time_origen)-3])
		origen_cen=int(time_origen[len(time_origen)-2:len(time_origen)])

		origen_year=int(origen[1])
		origen_month=int(origen[2][0:2])
		origen_day=int(origen[2][len(origen[2])-2:len(origen[2])])
		#print "print_year", origen_year
		#print "print_month", origen_month
		#print "origen_day", origen_day
		#print "origen_hora", origen_hora
		#print "origen_min", origen_min
		#print "origen_seg", origen_seg
		#print "origen_cen", origen_cen




		for a in arreglo:
			if len(a[0])==24:
				#print "primera opcion"
				time_pick=(a[0][len(a[0])-9:len(a[0])])
				pha_time= (a[0][len(a[0])-15:len(a[0])])
				#print "time_pick", (a[0][len(a[0])-15:len(a[0])])
				pick_hora=int(time_pick[0:2])
				pick_min=int(time_pick[2:4])
				pick_seg=int(time_pick[len(time_pick)-5:len(time_pick)-3])
				pick_cen=float(time_pick[len(time_pick)-3:len(time_pick)])
			else:
				#print "else"
				time_pick=(a[0][15:24])
				pha_time=(a[0][9:24])
				#print "time_pick", (a[0][9:24])
				#time_pick=float(a[0][len(a[0])-9:len(a[0])])
				pick_hora=int(time_pick[0:2])
				pick_min=int(time_pick[2:4])
				pick_seg=int(time_pick[len(time_pick)-5:len(time_pick)-3])
				pick_cen=float(time_pick[len(time_pick)-3:len(time_pick)])
			
			#060127113205.59

			phase_year=int("20"+pha_time[0:2])
			phase_mont=int(pha_time[2:4])
			phase_day=int(pha_time[4:6])
			phase_hora=int(pha_time[6:8])
			phase_min=int(pha_time[8:10])
			phase_seg=int(pha_time[10:12])
			phase_cen=int(pha_time[13:15])

			#print "phase_year", phase_year
			#print "phase_mont", phase_mont
			#print "phase_day", phase_day
			#print "phase_hora", phase_hora
			#print "phase_min", phase_min
			#print "phase_seg", phase_seg
			#print "phase_cen", phase_cen


			origen_t=datetime(origen_year,origen_month, origen_day, origen_hora, origen_min, origen_seg, origen_cen)

			phase_t=datetime(phase_year, phase_mont, phase_day, phase_hora, phase_min, phase_seg, phase_cen)


			print ("*******************************************")

			inicio_times=time_seg()
			
			time_end=phase_t-origen_t
			time_end=str(time_end)
			t_index=time_end.find(":")

			time_result=float(time_end[t_index+4:len(time_end)])
			#time_result="{0:.2f}".format(time_result-0.01)

			print (origen_t, "\t", phase_t)

			print ("time is: ", time_result)
			time_result=inicio_times.main(phase_hora, phase_min, phase_seg, phase_cen, origen_hora, origen_min, origen_seg, origen_cen)

			print ("********************************************")
			

			
			if not os.path.exists(path_salve):
				os.makedirs(path_salve)


			
			#fo = open('/home/carlos/Escritorio/datos_time/'+model+'/id_'+origen[0]+'.txt', 'a')
			fo = open(path_salve+'id_'+origen[0]+'.txt', 'a')
			#if result_seg < 30 and float(origen[7]) < 2:
				#time=result_seg+result_cen
			print (a[0][0:4], time_result, origen[7], 'P', time_end)
			fo.write(str(a[0][0:4])+'   '+str(time_result)+' '+str(origen[7])+'\n')
			
			

#inicio =Data_Times_HypDD()
#opc=1
#model="codex_16"
#inicio.main_data_times_hyp(model, opc)