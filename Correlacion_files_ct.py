# analiza correlacion entre dos muestras
#formatos SAC

#====================Librerias===================================================
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as waves
import math
import obspy
class Correlacion_files_ct():
	def main(self, path1, path2):
		#==========================Lectura de archivos===================================
		#path1= "/home/carlos/Dropbox/Catalogo_2006_2007/2006/027.11.32/027.11.32.ALPI.HHZ.SAC"
		#path1='/home/carlos/Escritorio/CODEX_2006/036/ZA.ZAPO..HHZ.M.2006.036.000000.SAC'
		#path1= "/home/carlos/Escritorio/Catalogo_CODEX_2006/2006.036.10.57/2006.036.10.57.ZA.COMA.HHZ.SAC"
		st1=obspy.read(path1)
		senal01=st1[0].data

		#path2= "/home/carlos/Dropbox/Catalogo_2006_2007/2006/028.13.45/028.13.45.ALPI.HHZ.SAC"

		st2=obspy.read(path2)
		senal02=st2[0].data
		print ('*************procedimiento**********************')
		# PROCEDIMIENTO
		tamano01 = len(senal01)
		tamano02 = len(senal02)


		print ('tamano01', tamano01, 'tamano02', tamano02)
		# Normaliza las senales





		if len(senal01)>=len(senal02):
			ex=math.log(len(senal01),2)
			lon_mx=len(senal01)

	
		else:
			ex=math.log(len(senal02),2)
			lon_mx=len(senal02)

		print (' longitud maxima', lon_mx)

		ex=math.ceil(ex)
		print ('exponente', ex)



		f0 = np.fft.fft(senal01, 2**int(ex))
		f1 = np.fft.fft(senal02, 2**int(ex))

		c=[]

		print (len(abs(f1)))
		print (len(abs(f0)))
		for i in range(int(2**ex)):
			c.append(f1[i].conjugate()*f0[i])
		#c=np.asarray(c)
		#====================== se realiza la transformada inversa para localizar el punto maximo de correlacion================
		print ('**********************Inversa*********************')
		R_xy = (np.fft.ifft(c))


		R_xy=np.asarray(R_xy)
		R_xy=abs(R_xy)
		t0=maximo=max(R_xy)
		#====================== se calcula la energia de cada senal================
		L2 = [(p)**2 for p in senal01]
		n=sum(L2)
		#n=np.linalg.norm(senal02)
		L1 = [(p1)**2 for p1 in senal02]
		m=sum(L1)
		#m=np.linalg.norm(senal01)
		print ('m', m, 'n', n)

		#====================== se normaliza la correlacion, valores de 0 a 1 ================
		for i in range(len(R_xy)):
			R_xy[i]=R_xy[i]/math.sqrt(n*m)


		maximo=max(R_xy)
		#indice=R_xy.index(maximo)

		print ('valos maximo de correlacion', maximo)
		#print 'indice donde encuentra la maxima correlacion', indice	
	
	
		return maximo
	
#iniciar=Correlacion()
#path1="/home/carlos/Dropbox/Catalogo_2006_2007/2006/027.11.32/027.11.32.ESPN.HHZ.SAC"
#path2="/home/carlos/Dropbox/Catalogo_2006_2007/2006/051.10.55/051.10.55.ESPN.HHZ.SAC"
#iniciar.main(path1, path2)