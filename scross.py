


#====================Librerias===================================================
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as waves
import math
import obspy
from scipy.signal import butter, lfilter
from Generar_Nombre import *
from time import time 
import time
import os
from obspy.core import UTCDateTime
#importa las clases
from Diezmado import *
from Procedimiento import*
from Genera_archivos import *
from Comprobar_Resultados import *
from PickerSAC import *
from Cut_Sac import *
#====================Librerias===================================================


class scross:




	def main(self, path1, path2, ruta_guardar_resultados, estaciones, year_an):
		#==========================Lectura de archivos===================================
		#tiempo_inicial = time()
		print (time.strftime("%H:%M:%S"))
		valor=19
		print ('dia', path1)
		print ('catalogo', path2)
		# lectura de los archivos sac
		st1=obspy.read(path1)

		self.senal01=st1[0].data

		inicia_diezmado=Diezmado()
		st2=obspy.read(path2)
		
		self.senal02=st2[0].data
		

		
		

		

		#==================nueva seccion=======================
		inicia_proceso = Procedimiento()
		# regresa dos arreglos, el primero que contiene los valores maximos de correlacion y el segundo los indices donde se encontro la maxima correlacion
		valores= inicia_proceso.proceso(inicia_diezmado.proceso(self.senal01, valor), inicia_diezmado.proceso(self.senal02, valor), valor)

		#*************************************************************************************************
		#*************************************************************************************************
		v_max=valores[0]
		v_indice= valores[1]

		for i in range(len(v_max)):

			indice_inicio_resp= v_indice[i]*4

			#Calcula la hora de la ventana del posible evento
			time_h=int(int(int((indice_inicio_resp)/100)/60)/60)
			#Calcula la minutos de la ventana del posible evento
			time_m=int(int(int((indice_inicio_resp)/100)/60)%60)
			#Calcula la minutos de la ventana del posible evento
			time_s=int(int((indice_inicio_resp)/100)%60)	
			
			#*************************************************************************************************
			#*************************************************************************************************

			#se verifica que tenga datos suficientes al inicio del dia (10 segundos antes o 1000 ntps antes)
			if indice_inicio_resp-1000>=0:
				#print 'IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII'
				#print 'indice inicial', indice_inicio_resp
				#print 'IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII'
				indice_fin_resp=(indice_inicio_resp)+(len(self.senal02)/2)

				inicio_genera=Generar_Nombre()
				#========================= Verifica formato de hora ======================================
				if len(str(time_h))==1:
					c_hora='0'+str(time_h)
				else:
					c_hora=str(time_h)

				#========================= Verifica formato de minutos======================================
				if len(str(time_m))==1:
					c_min='0'+str(time_m)
				else:
					c_min=str(time_m)
				#======================== Verifica  year de analisis ========================================
				if path1.find('2006')!=-1:
						
					year='2006'
					path1_dia=path1.rfind('2006')
					
					#======================== Verifica formato de dia ========================================
					c_dia=path1[path1_dia+5:path1_dia+8]
					if len(str(c_dia))==1:
						c_dia='0'+str(c_dia)
					else:
						c_dia=str(c_dia)
				elif path1.find('2007')!=-1:
					year='2007'
					path1_dia=path1.rfind('2007')
					
					#======================== Verifica formato de dia ========================================
					c_dia=path1[path1_dia+5:path1_dia+8]
					if len(str(c_dia))==1:
						c_dia='0'+str(c_dia)
					else:
						c_dia=str(c_dia)												
				else:
					year=year_an
					
					#======================== Verifica formato de dia ========================================
					path1_dia=path1.rfind(year_an)
					c_dia=path1[path1_dia+5:path1_dia+8]
					if len(str(c_dia))==1:
						c_dia='0'+str(c_dia)
					else:
						c_dia=str(c_dia)				

				print ('dia seleccionado', c_dia)

				path_salve_dia=ruta_guardar_resultados+'/'+c_dia
				nombre_path_resultados=path_salve_dia+'/'+year+'.'+c_dia+'.'+c_hora
				Inicia_guardar_archivos=Genera_archivos()

				

				if time_s-20<=0:
					aux_s=60+(time_s-20)
					aux_m=time_m-1
				else:
					aux_s=time_s-20
					aux_m=time_m
				if time_s+20>59:
					aux_ss=(time_s+20)-60
					aux_mm=time_m+1
				else:
					aux_ss=time_s+20
					aux_mm=time_m

				
				resp_archivo_existe1=0
				nombre_guardar=inicio_genera.main(path1, str(time_h), str(time_m), str(time_s), estaciones, year_an)
				aa= nombre_path_resultados+'/'+nombre_guardar
				#print '-----------------------------'
				print ('Posible Candidato: \n', aa)
				#print '-----------------------------'
				if aux_m<time_m:
 					for j in range(aux_s, 60):
 						
 						nombre_guardar_aux=inicio_genera.main(path1, str(time_h), str(aux_m), str(j), estaciones, year_an)
 						verifica_nombre=nombre_path_resultados+'/'+nombre_guardar_aux

 						if os.path.isfile(verifica_nombre):
 							print ('archivo:',  verifica_nombre,' existe')
 							resp_archivo_existe1=-1
 							break
 						else:
 							resp_archivo_existe1=1
 					if resp_archivo_existe1 !=-1:
 						for j in range(time_s+20):
 							#ruta=inicio_genera.main(path1, str(time_h), str(time_m), str(time_s))
 							nombre_guardar_aux=inicio_genera.main(path1, str(time_h), str(time_m), str(j), estaciones, year_an)
 							verifica_nombre=nombre_path_resultados+'/'+nombre_guardar_aux

 							if os.path.isfile(verifica_nombre):
 								print ('archivo:',  verifica_nombre,' existe')
 								resp_archivo_existe1=-1
 								break
 							else:
 								resp_archivo_existe1=1						
				else:
					resp_archivo_existe1=1


				resp_archivo_existe2=0
			
				if time_s-20>=0 and time_s+20<60:
					for j in range(time_s-20, time_s+20):
 						nombre_guardar_aux=inicio_genera.main(path1, str(time_h), str(time_m), str(j), estaciones, year_an)
 						verifica_nombre=nombre_path_resultados+'/'+nombre_guardar_aux

 						if os.path.isfile(verifica_nombre):

 							resp_archivo_existe2=-1
 							break
 						else:
 							resp_archivo_existe2=1

				else:
					resp_archivo_existe2=1	

				#print 'resultado respuesta2', resp_archivo_existe2
				resp_archivo_existe3=0										
				
				if aux_mm>time_m:

					for j in range(time_s-20, 60):
 						nombre_guardar_aux=inicio_genera.main(path1, str(time_h), str(time_m), str(j), estaciones, year_an)
 						verifica_nombre=nombre_path_resultados+'/'+nombre_guardar_aux
 						if os.path.isfile(verifica_nombre):
 							resp_archivo_existe3=-1
 							break
 						else:
 							resp_archivo_existe3=1




					if resp_archivo_existe3 != -1:		
						for j in range(aux_ss):
 							nombre_guardar_aux=inicio_genera.main(path1, str(time_h), str(aux_mm), str(j), estaciones, year_an)
 							verifica_nombre=nombre_path_resultados+'/'+nombre_guardar_aux
 							if os.path.isfile(verifica_nombre):
 								resp_archivo_existe3=-1
 								break
 							else:
 								resp_archivo_existe3=1
				else:
					resp_archivo_existe3=1
				
				#Si resp_archivo_existe es igual a 1 el archivo no existe y se trata de un nuevo evento
				if resp_archivo_existe1 == 1 and resp_archivo_existe2 == 1 and resp_archivo_existe3 == 1:

					#++++++++++++++++++++++++++++++++++++++++++++++++++
					print ('***Entra a verificar********')
					#================== Genera un sismograma en formato SAC para su posterior analisis ============================================
					Indice_inicial=indice_inicio_resp-200
					Indice_final=(indice_inicio_resp)+len(self.senal02)+1200
					inicio_cut=Cut_Sac()
					#print '****CHECAR LOS PARAMETROS QUE MANDA A LA FUNCION CUT_SAC****'
					path_aux=ruta_guardar_resultados+'/'+nombre_guardar
					inicio_cut.CutWindows(path1, path_aux, Indice_inicial, Indice_final)

									
					nombre_final=ruta_guardar_resultados+'/'+nombre_guardar
					#print nombre_final
					#=========================== Inicia la creacion de la clase para la busqueda de la onda P =====================================
					iniciaPicker=PickerSAC()
					#=========================== Termina la creacion de la clase para la busqueda de la onda P =====================================
					#=========================== Comienza el analisis de la onda P =================================================================
					opc=-2
					respuesta_picker=iniciaPicker.main(nombre_final, opc)
					#print respuesta_picker
					if respuesta_picker != -1:
						print ('********************************************************************')
						print ('********************************************************************')
						#print nombre_guardar
						#print 'archivo no existe'
						#nombre_guardar=inicio_genera.main(path1, str(time_h), str(time_m), str(time_s))
						print ('*+*+*+*+*+*+*+*+*+*+*+')
						print ('Guarda Archivo',nombre_guardar)
						print ('*+*+*+*+*+*+*+*+*+*+*+')


						busqueda_hhz=path1.find('HHZ.')
						path_HHE=path1[0:busqueda_hhz]+'HHE'+path1[busqueda_hhz+3:len(path1)]
						path_HHN=path1[0:busqueda_hhz]+'HHN'+path1[busqueda_hhz+3:len(path1)]
						print ('path_E', path_HHE)
						print ('path_N', path_HHN)

						Busqueda_nombreGuardar=nombre_guardar.find('HHZ.')
						nombre_guardar_E=nombre_guardar[0:Busqueda_nombreGuardar]+'HHE'+nombre_guardar[Busqueda_nombreGuardar+3:len(nombre_guardar)]
						nombre_guardar_N=nombre_guardar[0:Busqueda_nombreGuardar]+'HHN'+nombre_guardar[Busqueda_nombreGuardar+3:len(nombre_guardar)]

						print ('nombre_e', nombre_guardar_E)
						print ('nombre_N', nombre_guardar_N)


						#============================ Crea ruta donde se guardaran los resultados del analisis ============================================
					
						if not os.path.exists(path_salve_dia):
							os.makedirs(path_salve_dia)
						# Verifica si la carpeta existe para guardar resultados, en otro caso genera l carpeta											
					

						if not os.path.exists(nombre_path_resultados):
							os.makedirs(nombre_path_resultados)
							path_aux_Z=nombre_path_resultados+'/'+nombre_guardar
							path_aux_N=nombre_path_resultados+'/'+nombre_guardar_N
							path_aux_E=nombre_path_resultados+'/'+nombre_guardar_E

							
							inicio_cut.CutWindows(path1, path_aux_Z, Indice_inicial, Indice_final)
							inicio_cut.CutWindows(path_HHE, path_aux_E, Indice_inicial, Indice_final)
							inicio_cut.CutWindows(path_HHN, path_aux_N, Indice_inicial, Indice_final)
					
						else:

							path_aux_Z=nombre_path_resultados+'/'+nombre_guardar
							path_aux_N=nombre_path_resultados+'/'+nombre_guardar_N
							path_aux_E=nombre_path_resultados+'/'+nombre_guardar_E
							inicio_cut.CutWindows(path1, path_aux_Z, Indice_inicial, Indice_final)
							inicio_cut.CutWindows(path_HHE, path_aux_E, Indice_inicial, Indice_final)
							inicio_cut.CutWindows(path_HHN, path_aux_N, Indice_inicial, Indice_final)							

					
						nombre_final=nombre_path_resultados+'/'+nombre_guardar_E
						opc=0	
						respuesta_picker_E=iniciaPicker.main(nombre_final, opc)
						#print respuesta_picker_E
						nombre_final=nombre_path_resultados+'/'+nombre_guardar_N	
						respuesta_picker_N=iniciaPicker.main(nombre_final, opc)
						#print respuesta_picker_N					
						#opcion=0 para guardar los datos en archivo txt y  regresa el dia en el que se encontro
						opcion=0
						Inicia_guardar_archivos.crea_nombre(path1, path2, nombre_path_resultados, nombre_guardar, Indice_inicial, Indice_final, v_max[i], opcion, respuesta_picker,respuesta_picker_E, respuesta_picker_N, estaciones, year_an )	
						#Inicia_guardar_archivos.crea_nombre(path1, path2, path_salve_dia, nombre_guardar,indice_inicio_resp-1000, (indice_inicio_resp)+len(self.senal02), v_max[i], opcion, respuesta_picker,respuesta_picker_E, respuesta_picker_N )	

						print ('********************************************************************')
						print ('********************************************************************')

						return c_dia
		


#iniciar=scross()
#iniciar.main()
	